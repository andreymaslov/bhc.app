<!--Header-->
<?php require_once('templates/header.php'); ?>

<!-- .main-content -->
<div class="content">

    <!--navbar header section-->
    <?php require_once('templates/navbar-header-small.php'); ?>

    <div id='oopss'>
        <div id='error-text'>
            <span>404</span>
            <p>СТРАНИЦА НЕ НАЙДЕНА</p>
            <p class='hmpg'><a href='index.php' class="back">На главную</a></p>
        </div>
    </div>
    <!-- Error content -->

    <!--Main menu list-->
    <?php require_once('templates/menu.php'); ?>

</div> <!-- / .main-content -->


<!--Footer-->
<?php require_once('templates/footer.php'); ?>






