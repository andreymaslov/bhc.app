$(document).ready(function () {

    //Documentation github.com/xoxco/jQuery-Tags-Input
    //Tags input init and other connected functions
    $('#tags').tagsInput({
        'height':'auto',
        'width':'100%',
        'defaultText':'введите теги через запятую',
        'placeholderColor': '#8898aa'
    });

    //init second input in full search page
    $('#tagsMinus').tagsInput({
        'height':'auto',
        'width':'100%',
        'defaultText':'исключить теги',
        'placeholderColor': '#8898aa'
    });

    var tagBtn = $('.tags-item'),
        removeTagBtn = $('#removeAllBtn'),
        tagsInput = $('#tags_tagsinput'),
        tabs = $('.search-nav-wrapper').find('a.nav-link');

    //add tag on click
    tagBtn.click(function() {
        var tagValue = $(this).html();
        $('#tags').addTag(tagValue);
    });

    //remove all tags
    removeTagBtn.click(function() {
        $('#tags').val('');
        tagsInput.find('span.tag').remove()
    });

    //clean input by click on tabs
    tabs.click(function(){
        $('#tags').val('');
        tagsInput.find('span.tag').remove()
    });

    // var gettags = $_GET('tags');
    // var gettext = $_GET('text');
    var gettags = $('tags');
    var gettext = $('text');
    if(gettags === false && gettext != false){
            $('#tabs-icons-text-1-tab').removeClass('active');
            $('#tabs-icons-text-1').removeClass('active');
            $('#tabs-icons-text-1').removeClass('show');

            $('#tabs-icons-text-2-tab').addClass('active');
            $('#tabs-icons-text-2').addClass('active');
            $('#tabs-icons-text-2').addClass('show');
    }

    //remove category from results list
    removeCatBtn = $('.remove-item');

    removeCatBtn.click(function(){
        $(this).closest('li.result-stat-item').toggleClass('disabled-category');
    });



    //Mmenu init

    $("#mmenu").mmenu({
        extensions 	: [ "position-right" ],
        navbar : {title : 'Навигация'}
    });


    // Library accordion menu
    // on click
    var Accordion = function (el, multiple) {
        this.el = el || {};
        // more then one submenu open?
        this.multiple = multiple || false;

        var dropdownlink = this.el.find('.dropdownlink');
        dropdownlink.on('click',
            {el: this.el, multiple: this.multiple},
            this.dropdown);
    };

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el,
            $this = $(this),
            //this is the ul.submenuItems
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            //show only one menu at the same time
            $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
        }
    }
    var accordion = new Accordion($('.library-menu'), true);

    // Library accordion menu
    // on enter
    var Accordion = function (el, multiple) {
        this.el = el || {};
        // more then one submenu open?
        this.multiple = multiple || false;

        var dropdownlink = this.el.find('.dropdownlink');

        dropdownlink.on('keyup',
            {el: this.el, multiple: this.multiple},
            this.dropdown);
    };

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el,
            $this = $(this),
            //this is the ul.submenuItems
            $next = $this.next();

        if (e.keyCode == 13) {
            $next.slideToggle();
            $this.parent().toggleClass('open');
        }

    }

    var accordion = new Accordion($('.library-menu'), true);


    // Custom File upload

    $('#file-upload').change(function () {
        var filepath = this.value;
        var m = filepath.match(/([^\/\\]+)$/);
        var filename = m[1];
        $('#filename').html(filename);

    });

    //Floating search show/hide
    var $floatingContainer = $('.floating-container'),
        $searchToggler = $('#searchToggler'),
        $closeButton = $('.close-modal');

    $searchToggler.click(function () {
        $floatingContainer.fadeToggle().toggleClass('floating-container-open');
        $searchToggler.fadeOut();
    })

    $(document).mouseup(function (e) { // click event to document
        if (!$floatingContainer.is(e.target) // if not container was clicked
            && $floatingContainer.has(e.target).length === 0) { // and its childrens ans icon toggler

            if ($floatingContainer.hasClass('floating-container-open')) {  //check if container has opening class
                $floatingContainer.fadeOut().removeClass('floating-container-open');  //hide container
                $searchToggler.fadeIn();
            }
        }
    });

    $closeButton.click(function(){

        $floatingContainer.fadeToggle().toggleClass('floating-container-open');
        $searchToggler.fadeIn();
    });

    var page = 2;

    $('#moreResults').click(function(){
        var targetpage = document.getElementById('targetpage').value + '&page=' + page;
        $.ajax({
            url: "/" + targetpage,
            type: 'post',
            async: true,
            cache: false,
            success: function (response) {
                if(response === "0"){
                    $('#moreResults').hide();
                }else {
                    $('#result-list').append(response);
                }
            }
        });
        page ++;
    });

    // Sticky search panel

    var nav = $('#navbar-main'),
        stickySearch = $("#sticky-search");

    function changeHeader(){
        if ($(window).innerWidth() > '992') {
            $(function(){
                createSticky(stickySearch);
            });
        } else {
            nav.addClass('headroom');
        }
    }

    function createSticky(sticky) {
        if (sticky.length > 0) {
            var	pos = sticky.offset().top,
                win = $(window);
            win.on("scroll", function() {
                if ( win.scrollTop() >= pos ) {
                    sticky.addClass("fixed-search");
                    nav.addClass("fixed-nav");
                    $('body').addClass("fixed-pt");
                } else {
                    sticky.removeClass("fixed-search");
                    nav.removeClass("fixed-nav");
                    $('body').removeClass("fixed-pt");
                }
            });
        }
    }

    changeHeader();

    // Headroom - show/hide navbar on scroll
    if($('.headroom')[0]) {
        var headroom  = new Headroom(document.querySelector(".headroom"), {
            offset: 50,
            tolerance : {
                up : 20,
                down : 10
            },
        });
        headroom.init();
    }

    // Back to top
    var $backToTop = $(".back-to-top");
    $backToTop.hide();

    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 300) {
            $backToTop.fadeIn();
        } else {
            $backToTop.fadeOut();
        }
    });

    $backToTop.on('click', function(e) {
        $("html, body").animate({scrollTop: 0}, 500);
    });

});


//close-open sticked search button
function opensearch(){
    if($('#sticky-search').hasClass('closed')){
        console.log(1);
        $('#sticky-search').removeClass('closed')
    }else{
        console.log(2);
        $('#sticky-search').addClass('closed')
    }
}
