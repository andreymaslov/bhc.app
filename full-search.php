<!--Header-->
<?php require_once('templates/header.php'); ?>

<!-- .main-content -->
<div class="content">

    <!--navbar header section-->
    <header class="header-global">
        <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
            <div class="navbar-container container">
                <!--Logo-->
                <a class="navbar-brand mr-lg-5" href="index.php">
                    <img src="./assets/img/logo.png">
                </a>

                <div class="navbar-togglers">
                    <!--Hamburger-->
                    <a href="#mmenu" class="btn-icon navbar-menu-btn" id="linkToggler">
                        <i class="demo-icon icon-menu" aria-hidden="true"></i>
                    </a>
                </div>

            </div>
        </nav>
    </header>
    <!--Main Search section -->
    <section class="main-search section section-top">
        <div class="container">
            <div class="row align-items-center justify-content-center">

                <div class="col-lg-12 text-center">
                    <h1 class="main-title">Документы в области прав человека для юристов и менеджеров
                        проектов</h1>
                    <h3 class="lead main-subtitle">Введите запрос и нажмите кнопку
                        "Искать"<br>Можно
                        воспользоваться <a href="instruction.php" class="bordered-link">инструкцией</a> по
                        составлению запроса</h3>
                </div>

                <!--Search input form-->
                <div class="col-sm-12">
                    <?php require_once('templates/full-search-module.php') ?>
                </div><!-- / Search input form-->

            </div> <!-- / .row-->
        </div>
    </section>

    <!--navbar header section-->
    <?php require_once('templates/popular-tags-module.php'); ?>


    <!-- Services section -->
    <?php require('templates/services-section.php') ?>

    <!--Support section-->
    <?php require('templates/support-section.php') ?>

    <!--Main menu list-->
    <?php require_once('templates/menu.php'); ?>

</div> <!-- / .main-content -->

<!--Footer-->
<?php require_once('templates/footer.php'); ?>






