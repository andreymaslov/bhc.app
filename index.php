<!--Header-->
<?php require_once('templates/header.php'); ?>

<!-- .main-content -->
<div class="content">

    <!--navbar header section-->
    <?php require_once('templates/navbar-header-big.php'); ?>

    <!--popular tags section-->
    <?php require_once('templates/popular-tags-module.php') ?>

    <!-- Services section -->
    <?php require('templates/services-section.php') ?>

    <!--Support section-->
    <?php require('templates/support-section.php') ?>

    <!--Main menu list-->
    <?php require_once('templates/menu.php'); ?>

</div> <!-- / .main-content -->

<!--Footer-->
<?php require_once('templates/footer.php'); ?>






