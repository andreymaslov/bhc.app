<!--Header-->
<?php require_once('templates/header.php'); ?>

<!-- .main-content -->
<div class="content">

    <!--navbar header section-->
    <?php require_once('templates/navbar-header-big.php'); ?>

    <div class="mb-large"></div>

    <!-- Library content -->
    <section class="section-document">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="library article-wrapper section-inner">
                        <div class="library-article-header">
                            <a href="#" class="category-name" tabindex="0">Международные договоры Республики Беларусь</a>
                            <h2 class="document-name" tabindex="0">Международный пакт об экономических, социальных и культурных
                                правах</h2>
                            <p class="article-table-header" tabindex="0">Содержание документа</p>
                        </div>

                        <ul class="article-list">

                            <?php for($i = 3; $i < 16; $i++) {
                                ?>

                                <li class="article-list-item" tabindex="0">
                                    <p>1-<?php echo $i ; ?>. Все народы для достижения своих целей могут свободно распоряжаться своими
                                        естественными богатствами и ресурсами без ущерба для каких-либо обязательств,
                                        вытекающих
                                        из международного экономического сотрудничества, основанного на принципе
                                        взаимной
                                        выгоды, и из международного права. Ни один народ ни в коем случае не может быть
                                        лишен
                                        принадлежащих ему средств существования. Ни один народ ни в коем случае не может
                                        быть
                                        лишен принадлежащих ему средств существования.</p>
                                </li>

                                <?php
                            };
                            ?>

                        </ul>
                    </div> <!-- / document-wrapper  section-inner-->
                </div>
                <div class="col-lg-4">
                    <div class="section-inner library-menu-wrapper">
                        <?php require_once('templates/library-menu.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Services section -->
    <?php require('templates/services-section.php') ?>

    <!--Support section-->
    <?php require('templates/support-section.php') ?>

    <!--Main menu list-->
    <?php require_once('templates/menu.php'); ?>

</div> <!-- / .main-content -->

<!--Footer-->
<?php require_once('templates/footer.php'); ?>