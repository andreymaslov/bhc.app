<!--Header-->
<?php require_once('templates/header.php'); ?>

<!-- .main-content -->
<div class="content">

    <!--navbar header section-->
    <?php require_once('templates/navbar-header-big.php'); ?>

    <div class="mb-large"></div>

    <!-- Library content -->
    <section class="section-document">
        <div class="container">
            <div class="row">

                <div class="col-lg-8">
                    <div class="library section-inner">
                        <h2 class="document-name">Международные договоры Республики Беларусь</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, animi aspernatur
                            commodi
                            nemo obcaecati omnis placeat quibusdam voluptas. Animi, quia.</p>
                        <p>В Этом месте будет описание категории на несколько строчек.
                            Это такая-то категория, которая возникла в результате многочисленных прений сторон.</p>
                        <p><strong>В ней содержатся следующие документы:</strong></p>
                        <ol class="library-documents-list">
                            <?php for($i = 0; $i < 6; $i++) {
                                ?>

                                <li>
                                    <a href="library-open.php">Международный пакт об экономических социальных и
                                    культурных правах!!!</a>
                                </li>

                            <?php
                                };
                            ?>
                        </ol>
                    </div> <!-- / document-wrapper  section-inner-->
                </div>
                <div class="col-lg-4 ">
                    <div class="section-inner library-menu-wrapper">
                        <?php require_once('templates/library-menu.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Services section -->
    <?php require('templates/services-section.php') ?>

    <!--Support section-->
    <?php require('templates/support-section.php') ?>

    <!--Main menu list-->
    <?php require_once('templates/menu.php'); ?>

</div> <!-- / .main-content -->

<!--Footer-->
<?php require_once('templates/footer.php'); ?>