<footer class="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <p class="footer-copyright">&copy; Все права защищены | <span><?php echo date("Y"); ?></span></p>
                </div>
                <div class="col-md-6">
                    <p class="footer-devs">
                        Разработка сайта <a href="https://arktur.solutions" target="_blank">Arktur Solutions</a> & <a href="https://maslov.work" target="_blank">Maslov</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="back-to-top">
    <i class="demo-icon icon-down-open-1" aria-hidden="true"></i>
</div>

<!-- Core -->

<script>

</script>

<script src="./assets/vendor/popper/popper.min.js"></script>
<script src="./assets/vendor/bootstrap/bootstrap.min.js"></script>
<script src="./assets/vendor/headroom/headroom.min.js"></script>
<!-- Optional JS -->
<script src="./assets/vendor/onscreen/onscreen.min.js"></script>
<script src="./assets/vendor/mmenu/dist/jquery.mmenu.all.js"></script>
<script src="./assets/js/jquery.tagsinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>

<!-- Argon JS -->
<script src="./assets/js/argon.js?v=1.0.1"></script>
<script src="./assets/js/common.js?v=1.0.1"></script>

<!--MODALS-->

</body>

</html>