<div class="full-search-panel">
    <form class="main-search-form" action="">
        <div class="row">
            <div class="col-sm-12">

                <!-- Tags search input-->
                <div class="main-search-input tags-input">
                    <input name='tags' id="tags" value='' autofocus >
                    <button class="tags--removeAllBtn" id="removeAllBtn" type="button" data-toggle="tooltip"
                            data-placement="top" title="" data-container="body" data-animation="true"
                            data-original-title="При нажатии поле ввода будет очищено" aria-label="удалить все введенные теги">
                        <span aria-hidden="true" class="times">&times;</span>
                    </button>
                </div>

                <p class="text-center text-white">Исключить теги</p>

                <!-- Tags search input-->
                <div class="main-search-input tags-input2 ">
                    <input name='tags2' id="tagsMinus" value='' autofocus>
                </div>

                 <div class="text-right ">
                     <a class="link-switcher" href="index.php">
                         <i class="demo-icon icon-search" aria-hidden="true"></i>
                         Обычный поиск
                     </a>
                 </div>


            </div>
            <div class="col-lg-12 text-center">
                <input class="btn btn-icon btn-md btn-primary search-submit-1" type="submit" value="Искать"
                       id="searchBtn">
            </div>
        </div>
    </form>
</div>