<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Searchin system for NGO">
    <meta name="author" content="Andrey Maslov">
    <meta name="theme-color" content="#172b4d">
    <title>Searchin system for NGO</title>
    <!-- Favicon -->
    <link href="fav.ico" rel="icon" type="image/png">

    <!-- Apple Touch Icon (at least 200x200px) -->
<!--    <link rel="apple-touch-icon" href="">-->

    <!-- To run web application in full-screen -->
    <meta name="apple-mobile-web-app-capable" content="yes">

    <!-- Status Bar Style (see Supported Meta Tags below for available values) -->
    <!-- Has no effect unless you have the previous meta tag -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link rel="stylesheet" href="assets/css/jquery.tagsinput.min.css">

    <!-- Argon CSS -->
    <link type="text/css" href="./assets/css/argon.css?v=1.1.3" rel="stylesheet">

    <link rel="stylesheet" href="assets/vendor/mmenu/dist/jquery.mmenu.css">
    <link rel="stylesheet" href="assets/vendor/mmenu/dist/extensions/positioning/jquery.mmenu.positioning.css">

    <script src="./assets/vendor/jquery/jquery.min.js"></script>

</head>

<body>