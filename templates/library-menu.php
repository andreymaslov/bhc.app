<p class="library-menu-title">Категории</p>
<ul class="library-menu" role="navigation" aria-label="список категорий и документов" >
    <li class="open has-children">
        <div class="dropdownlink" tabindex="0">
            <i class="demo-icon icon-down-open-1" aria-hidden="true"></i>
            Международные договоры Республики Беларусь
        </div>
        <ul class="submenuItems" style="display: block">
            <li class="active-item"><a href="#" tabindex="0">Международный пакт</a></li>
            <li><a href="#" tabindex="0">Международный пакт о гражданских и политических правах</a></li>
            <li><a href="#" tabindex="0">Конвенция о ликвидации всех форм дискриминации в отношении женщин</a></li>
            <li><a href="#" tabindex="0">Международная конвенция о ликвидации всех форм расовой дискриминации</a></li>
            <li><a href="#" tabindex="0">Конвенция против пыток и других жестоких, бесчеловечных или наказания</a></li>
            <li class="has-children">
                <a href="#" tabindex="0">Промежуточный пункт меню. Ссылка</a>
                <i class="dropdownlink demo-icon icon-down-open-1" aria-hidden="true"></i>
                <ul class="submenuItems">
                    <li><a href="#" tabindex="0">Международный пакт об экономических, социальных и культурных правах</a></li>
                    <li><a href="#" tabindex="0">Международный пакт о гражданских и политических правах</a></li>
                    <li><a href="#" tabindex="0">Конвенция о правах ребенка</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="has-children">
        <div class="dropdownlink" tabindex="0">
            <i class="demo-icon icon-down-open-1" aria-hidden="true"></i>
            Внутренние договоры Республики Беларусь
        </div>
        <ul class="submenuItems">
            <li><a href="#" tabindex="0">Международный пакт об экономических, социальных и культурных правах</a></li>
            <li class="has-children">
                <a href="#" tabindex="0">Международная конвенция о ликвидации всех форм расовой дискриминации</a>
                <i class="dropdownlink demo-icon icon-down-open-1" aria-hidden="true"></i>
                <ul class="submenuItems">
                    <li><a href="#" tabindex="0">Международный пакт об экономических, социальных и культурных правах</a></li>
                    <li><a href="#" tabindex="0">Международный пакт о гражданских и политических правах</a></li>
                    <li><a href="#" tabindex="0">Конвенция о правах ребенка</a></li>
                </ul>
            </li>
            <li><a href="#" tabindex="0">Конвенция против пыток и других жестоких, бесчеловечных или наказания</a></li>
            <li><a href="#" tabindex="0">Конвенция о правах ребенка</a></li>
        </ul>
    </li>
    <li class="has-children">
        <div class="dropdownlink" tabindex="0">
            <i class="demo-icon icon-down-open-1" aria-hidden="true"></i>
            Договоры всех стран мира
        </div>
        <ul class="submenuItems">
            <li><a href="#" tabindex="0">Международный пакт об экономических, социальных и культурных правах</a></li>
            <li><a href="#" tabindex="0">Международный пакт о гражданских и политических правах</a></li>
            <li><a href="#" tabindex="0">Конвенция о правах ребенка</a></li>
        </ul>
    </li>
    <li class="has-children">
        <div class="dropdownlink" tabindex="0">
            <i class="demo-icon icon-down-open-1" aria-hidden="true"></i>
            Рекомендации ООН для всех православных стран
        </div>
        <ul class="submenuItems">
            <li><a href="#" tabindex="0">Международный пакт об экономических, социальных и культурных правах</a></li>
            <li><a href="#" tabindex="0">Международный пакт о гражданских и политических правах</a></li>
            <li><a href="#" tabindex="0">Конвенция о ликвидации всех форм дискриминации в отношении женщин</a></li>
            <li><a href="#" tabindex="0">Международная конвенция о ликвидации всех форм расовой дискриминации</a></li>
            <li><a href="#" tabindex="0">Конвенция против пыток и других жестоких, бесчеловечных или наказания</a></li>
            <li><a href="#" tabindex="0">Конвенция о правах ребенка</a></li>
        </ul>
    </li>
</ul>