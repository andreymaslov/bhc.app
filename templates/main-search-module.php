<div class="main-search-panel">
    <form class="main-search-form">
        <div class="row">
            <div class="col-sm-12">

                <div class="search-nav-wrapper">
                    <ul class="nav search-nav-tabs flex-md-row" id="tabs-icons-text" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link left-tab active"
                               id="tabs-icons-text-1-tab" data-toggle="tab"
                               href="#tabs-icons-text-1" role="tab"
                               aria-controls="tabs-icons-text-1" aria-selected="true">Поиск по тегам</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link right-tab" id="tabs-icons-text-2-tab"
                               data-toggle="tab" href="#tabs-icons-text-2" role="tab"
                               aria-controls="tabs-icons-text-2" aria-selected="false">Поиск по тексту</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="tabs-icons-text-1"
                         role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                        <!-- Tags search input-->
                        <div class="main-search-input tags-input">
                            <input id="tags" value="" name="tags">
                            <button class="tags--removeAllBtn" id="removeAllBtn" type="button" data-toggle="tooltip"
                                    data-placement="top" title="" data-container="body" data-animation="true"
                                    data-original-title="При нажатии поле ввода будет очищено" aria-label="удалить все введенные теги">
                                <span aria-hidden="true" class="times">&times;</span>
                            </button>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel"
                         aria-labelledby="tabs-icons-text-2-tab">

                        <!-- Text search input-->
                        <div class="main-search-input text-input">
                            <input type="text" class="form-control" placeholder="ведите сюда текст">
                        </div>
                    </div>
                    <!--  sticky submit btn -->
                    <button class="btn btn-primary search-submit-sticky" type="submit" data-toggle="confirmation">
                        <i class="demo-icon icon-search" aria-hidden="true"></i>
                    </button>

                </div>



                <div class="text-right link-switcher flex-switchers">
                    <div class="custom-control custom-checkbox mb-3">
                        <input class="custom-control-input" id="exactSearch" type="checkbox">
                        <label class="custom-control-label" for="exactSearch">Точный поиск</label>
                    </div>
                    <div>
                        <a class="" href="full-search.php">
                            <i class="demo-icon icon-sliders" aria-hidden="true"></i>
                            Расширенный поиск
                        </a>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 text-center">
                <input class="btn btn-icon btn-md btn-primary search-submit-1" type="submit" value="Искать"
                       id="searchBtn" data-toggle="confirmation">
            </div>
        </div>
    </form>
</div>


<script>

    // $(document).ready(function(){
    //     // Confirmation on some button click
    //
    //     $(document).ready(function () {
    //         $("#removeAllBtn").popConfirm({
    //             title: 'Confirmation',
    //             content: 'Are you really sure ?',
    //             placement: 'right',
    //             container: 'body',
    //             yesBtn: 'Yes',
    //             noBtn: 'No'
    //         });
    //     });
    //
    // })


</script>

<script>

</script>