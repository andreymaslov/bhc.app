<header class="header-global">
    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light " >
        <div class="navbar-container container">
            <!--Logo-->
            <a class="navbar-brand mr-lg-5" aria-label="Перейти на главную" href="index.php">
                <img src="./assets/img/home.png" aria-hidden="true" alt="Домой">
            </a>
            <div class="navbar-togglers">
                <!--Hamburger-->
                <a href="#mmenu" class="btn-icon navbar-menu-btn" aria-label="Перейти к навигации" id="linkToggler">
                    <i class="demo-icon icon-menu" aria-hidden="true"></i>
                    <!--<span class="menu-open" id="btnToggler"></span>-->
                </a>
            </div>
        </div>
    </nav>
</header>
<!--Main Search section -->
<section class="main-search section section-top">
    <div class="container">
        <div class="row align-items-center justify-content-center">

            <div class="col-lg-12 text-center">
                <h1 class="main-title">Документы в области прав человека для юристов и менеджеров
                    проектов</h1>
                <h3 class="lead main-subtitle">Введите запрос и нажмите кнопку
                    "Искать"<br>
                    Можно воспользоваться <a href="instruction.php" class="bordered-link">инструкцией</a> по
                    составлению запроса</h3>
            </div>
        </div>
    </div>
    <div id="sticky-search">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <!--Search input form-->
                <div class="col-sm-12">

                    <?php require_once('templates/main-search-module.php') ?>


                </div><!-- / Search input form-->

            </div> <!-- / .row-->
        </div>
        <div id="open-search" onclick="opensearch()"></div>
    </div>
</section>
