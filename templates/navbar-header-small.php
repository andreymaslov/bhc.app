<header class="header-global">
    <nav id="navbar-main" class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
        <div class="navbar-container container">
            <!--Logo-->
            <a class="navbar-brand mr-lg-5" href="index.php">
                <img src="./assets/img/home.png" alt="Домой">
            </a>
            <div class="navbar-togglers">
                <!--Search toggler-->
                <a href="#" class="btn-icon navbar-menu-btn" id="searchToggler">
                    <i class="demo-icon icon-search" aria-hidden="true"></i>
                </a>
<!--                Hamburger-->
                <a href="#mmenu" class="btn-icon navbar-menu-btn" id="linkToggler">
                    <i class="demo-icon icon-menu" aria-hidden="true"></i>
                </a>



            </div>
        </div>
    </nav>
</header>

<!-- floating search -->
<div class="section section-top mb-large">
    <div class="container floating-container">
        <div class="close-modal">
            <span>&times;</span>
        </div>
        <?php require('templates/main-search-module.php') ?>
    </div>
</div>