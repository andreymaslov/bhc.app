<section class="section-offer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="offer-wrapper section-inner">
                    <div class="section-inner-title offer-title">
                        <h2>Посоветуйте нам правовой документ</h2>
                        <p class="lead">
                            Для того, чтобы рассказать о документе, вам нужно заполнить форму и отправить ее
                        </p>
                    </div>

                    <form class="offer-form" id="offer" data-toggle="validator" data-disable="false">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="name">Введите ваше имя (обязательное поле)</label>
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="demo-icon icon-user" aria-hidden="true"></i></span>
                                        </div>
                                        <input id="name" class="form-control" placeholder="Имя*" type="text"
                                               autofocus required autocomplete="name" autocorrect="off">
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <label for="email">Введите ваш email</label>
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="demo-icon icon-envelope-open" aria-hidden="true"></i></span>
                                        </div>
                                        <input id="email" class="form-control" placeholder="Email" type="email"
                                               autocapitalize="off" autocomplete="email">
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <label for="tel">Введите ваш номер телефона</label>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="demo-icon icon-phone" aria-hidden="true"></i></span>
                                        </div>
                                        <input id="tel" class="form-control" placeholder="Номер телефона"
                                               type="tel" autocomplete="tel">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="message">Напишите нам что-нибудь</label>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <textarea id="message" class="form-control"
                                                  placeholder="Сообщение ..."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div><!-- / .row-->

                        <div class="row">
                            <div class="col-md-6">
                                <label>Можете прикрепить файл к сообщению</label>
                                <div class="upload-wrapper">
                                    <label for="file-upload" aria-label="выбрать файл на компьютере"><i class="demo-icon icon-upload" aria-hidden="true"></i> Искать
                                        <input type="file" id="file-upload">
                                        <span></span>
                                    </label>
                                    <span id="filename">Выберите файл</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="file-url" >Ссылка на документ</label>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="demo-icon icon-network" aria-hidden="true"></i></span>
                                        </div>
                                        <input id="file-url" class="form-control" placeholder="Ссылка на документ"
                                               type="url"
                                               autocapitalize="off">
                                    </div>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div> <!-- / .row-->

                        <div class="row">
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="text-center">
                                        <input type="submit" class="btn btn-primary w100"
                                               value="Поделиться информацией" aria-label="Отправить информацию">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="confPolitix"
                                                   data-error="Для того, чтобы отправить сообщение, вы должны согласиться с политикой конфиденциальности."
                                                   required>
                                            Согласен с <a href="#" class="bordered-link" data-toggle="modal"
                                                          data-target="#confPolitixModal" aria-label="ссылка на политику конфиденциальности">политикой
                                                конфиденциальности</a>
                                        </label>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>

                            </div>
                        </div> <!-- / .row-->

                    </form>

                </div> <!-- / offer-wrapper  section-inner-->
            </div>
        </div>
    </div>
</section>


<script>
    //Contact form validation


</script>

<!-- Modal confidential info -->

<?php require('modals.php'); ?>
