<!-- Popular tags section -->
<div class="position-relative tags">
    <div class="container">
        <div class="row align-items-center justify-content-center">

            <!--Popular tags / All tags-->
            <div class="col-sm-12">
                <p tabindex="0" aria-label="Группа популярных тегов" class="tags-list-title">Популярные теги</p>
                <div class="tags-popular tags-wrapper">
                    <button class="btn tags-item tag">Защита детей!</button>
                    <button class="btn tags-item tag">ЛГБТ</button>
                    <button class="btn tags-item tag">Права человека</button>
                    <button class="btn tags-item tag">ЛДПР</button>
                    <button class="btn tags-item tag">Права йетти</button>
                    <button class="btn tags-item tag">Защита детей</button>
                    <button class="btn tags-item tag">Права нонконформистов</button>
                    <button class="btn tags-item tag">Защита детей!</button>
                    <button class="btn tags-item tag">ЛГБТ</button>
                    <button class="btn tags-item tag">Права человека</button>
                    <button class="btn tags-item tag">ЛДПР</button>
                    <button class="btn tags-item tag">Права йетти</button>
                    <button class="btn tags-item tag">Права нонконформистов</button>
                    <button class="btn tags-item tag">Защита детей!</button>
                    <button class="btn tags-item tag">ЛГБТ</button>
                    <button class="btn tags-item tag">Права человека</button>
                    <button class="btn tags-item tag">Права йетти</button>
                    <button class="btn tags-item tag">Защита детей</button>
                    <button class="btn tags-item tag">Права нонконформистов</button>
                    <button class="btn tags-item tag">Защита детей!</button>
                    <button class="btn tags-item tag">ЛГБТ</button>
                    <button class="btn tags-item tag">Права человека</button>
                    <button class="btn tags-item tag">ЛДПР</button>
                    <button class="btn tags-item tag">Права йетти</button>
                    <button class="btn tags-item tag">Защита детей</button>
                    <button class="btn tags-item tag">Права нонконформистов</button>
                    <button class="btn tags-item tag">Защита детей!</button>
                    <button class="btn tags-item tag">ЛГБТ</button>
                    <button class="btn tags-item tag">Права человека</button>
                    <button class="btn tags-item tag">ЛДПР</button>
                    <button class="btn tags-item tag">Права йетти</button>
                    <button class="btn tags-item tag">Права нонконформистов</button>
                    <button class="btn tags-item tag">Защита детей!</button>
                    <button class="btn tags-item tag">ЛГБТ</button>
                    <button class="btn tags-item tag">Права человека</button>
                    <button class="btn tags-item tag">Права йетти</button>
                    <button class="btn tags-item tag">Защита детей</button>
                    <button class="btn tags-item tag">Права нонконформистов</button>
                </div>
                <div class="text-right w100">
                    <a class="link-switcher" href="all-tags-page.php">
                        <i class="demo-icon icon-globe" aria-hidden="true" ></i>
                        Все теги
                    </a>
                </div>
            </div>

        </div> <!-- / .row-->
    </div> <!-- / .container-->
    <div class="section-triangle"></div>
</div>