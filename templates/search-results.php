<div class="section-result-stat" id="resultStat">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="result-stat-lead lead" tabindex="0">Найдено: <span>34 статьи</span> в <span>3 категориях</span></p>
                <ul class="result-stat-list">
                    <li class="result-stat-item">
                        <p>Международные договоры Республики Беларусь</p>
                        <p>20
                            <button class="remove-item" data-toggle="tooltip"
                                    data-placement="top" data-container="body" data-animation="true"
                                    data-original-title="Удалить категорию из выдачи" aria-label="Удалить категорию из выдачи">
                                <span>&times;</span>
                            </button>
                        </p>

                    </li>
                    <li class="result-stat-item">
                        <p>Конвенция против пыток и других жестоких, бесчеловечных или унижающих достоинство видов
                            обращения и наказания</p>
                        <p>13
                            <button class="remove-item" data-toggle="tooltip"
                                    data-placement="top" data-container="body" data-animation="true"
                                    data-original-title="Удалить категорию из выдачи" aria-label="Удалить категорию из выдачи">
                                <span>&times;</span>
                            </button>
                        </p>
                    </li>
                    <li class="result-stat-item">
                        <p>Международный пакт о гражданских и политических правах</p>
                        <p>1
                            <button class="remove-item" data-toggle="tooltip"
                                    data-placement="top" data-container="body" data-animation="true"
                                    data-original-title="Удалить категорию из выдачи" aria-label="Удалить категорию из выдачи">
                                <span>&times;</span>
                            </button>
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="mb-large"></div>

<section class="section-result">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="result-wrapper section-inner">
                    <ol class="result-list">

                        <?php

                            for ($i = 0; $i <= 10; $i++) {
                                ?>
                                <!-- results item -->
                                <li class="result-item">
                                    <div class="result-item-content">
                                        <div class="result-item-cat result-item-part">
                                            <span>Категория:</span>
                                            <a href="library.php" class="category-name" data-toggle="tooltip" data-placement="top"
                                               data-container="body" data-animation="true"
                                               data-original-title="Тут будет краткое и красивое описание этой категории документов на некоторое количество слов. Оно будет появляться по наведению на вопросик в конце названия категории. Скорее всего, это будет понятно пользователю. ">Международные
                                                договоры Республики Беларусь <sup class="far fa-question-circle"></sup>
                                            </a>
                                        </div>
                                        <div class="result-item-doc result-item-part">
                                            <span>Документ:</span>
                                            <a href="library-open.php" class="document-name">Международный пакт об экономических,
                                                социальных и
                                                культурных правах
                                            </a>
                                        </div>
                                        <div class="result-item-text result-item-part">
                                            <a href="library-open.php" class="result-text-link">1-1. Все народы имеют право на
                                                самоопределение. В силу этого права они свободно
                                                устанавливают свой политический статус
                                                и свободно обеспечивают свое экономическое, социальное и культурное
                                                развитие
                                            </a>
                                        </div>
                                        <div class="result-item-tags result-item-part">
                                            <span class="tag">Права детей</span>
                                            <span class="tag">Права детей</span>
                                            <span class="tag">Права детей</span>
                                            <span class="tag">Права детей</span>
                                        </div>
                                    </div>
                                </li> <!-- / results item -->
                            <?php }

                        ?>

                        <!--More results. Only for development-->
                        <div class="more-results">
                            <?php

                            for ($i = 0; $i <= 10; $i++) {
                                ?>
                                <!-- results item -->
                                <li class="result-item">
                                    <div class="result-item-content">
                                        <div class="result-item-cat result-item-part">
                                            <span>Категория:</span>
                                            <a href="library.php" class="category-name" data-toggle="tooltip" data-placement="top"
                                               title=""
                                               data-container="body" data-animation="true"
                                               data-original-title="Тут будет краткое и красивое описание этой категории документов на некоторое количество слов. Оно будет появляться по наведению на вопросик в конце названия категории. Скорее всего, это будет понятно пользователю. ">Международные
                                                договоры Республики Беларусь <sup class="far fa-question-circle"></sup>
                                            </a>
                                        </div>
                                        <div class="result-item-doc result-item-part">
                                            <span>Документ:</span>
                                            <a href="document.php" class="document-name">Международный пакт об экономических,
                                                социальных и
                                                культурных правах
                                            </a>
                                        </div>
                                        <div class="result-item-text result-item-part">
                                            <a href="#" class="result-text-link">1-1. Все народы имеют право на
                                                самоопределение. В силу этого права они свободно
                                                устанавливают свой политический статус
                                                и свободно обеспечивают свое экономическое, социальное и культурное
                                                развитие
                                            </a>
                                        </div>
                                        <div class="result-item-tags result-item-part">
                                            <span class="tag">Права детей</span>
                                            <span class="tag">Права детей</span>
                                            <span class="tag">Права детей</span>
                                            <span class="tag">Права детей</span>
                                        </div>
                                    </div>
                                </li> <!-- / results item -->
                            <?php }

                            ?>
                        </div>

                    </ol>
                    <button class="btn btn-1 btn-outline-main more-btn" type="button" id="moreResults">Показать еще</button>
                </div> <!-- / result-wrapper-->
            </div>
        </div>
    </div>
</section>

<script>
    jQuery(document).ready(function(){
        // More search results by click/ Only for development

        var $moreBtn = $('#moreResults'),
            $moreContainer = $('.more-results');

        $moreBtn.click(function(){
            $moreContainer.slideDown();
            $moreBtn.hide();
        })
    })
</script>