<section class="section section-services" id="section-services">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <!-- Basic elements -->
                <h4 class="mb-5 text-center services-title">
                    Вы можете воспользоваться инструкцией, библиотекой ресурсов<br> или посоветовать нам
                    документ на
                    размещение
                </h4>

                <div class="services-wrapper">
                    <div class="row justify-content-between">
                        <div class="col-md-3 col-sm-6">
                            <div class="services-box">
                                <a class="services-link" href="about.php">
                                    <img class="img-responsive" aria-hidden="true" src="assets/img/speaker.png" alt="о проекте">
                                    <h5>О проекте</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="services-box">
                                <a class="services-link" href="instruction.php">
                                    <img class="img-responsive" aria-hidden="true" src="assets/img/study.png" alt="Инструкция">
                                    <h5>Инструкция</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="services-box">
                                <a class="services-link" href="offer.php">
                                    <img class="img-responsive" aria-hidden="true" src="assets/img/document.png" alt="Посоветовать документ">
                                    <h5>Посоветовать документ</h5>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="services-box">
                                <a class="services-link" href="library.php">
                                    <img class="img-responsive" aria-hidden="true" src="assets/img/finger.png" alt="Все документы">
                                    <h5>Все документы</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div> <!-- .row -->
        <div class="row">
            <div class="col-lg-12">
                <p class="text-center">Тут можно разместить еще какой нибудь информационный текст с картинками</p>
            </div>
        </div>
    </div>
</section>