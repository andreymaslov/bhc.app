<section class="section section-support">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <h3 class="support-title text-center">Мы поддерживаем</h3>
                <a class="support-logo" href="http://www.by.undp.org/content/belarus/ru/home/post-2015/sdg-overview.html" target="_blank">
                    <img class="img-responsive" src="assets/img/cyr-logo.png" alt="Логотип целей устойчивого развития">
                </a>
            </div>
            <div class="col-lg-8">
                <div class="support-all">
                    <img class="img-responsive" src="assets/img/cyr-all.png" alt="Графика целей устойчивого развития">
                </div>
            </div>
        </div>
    </div>
</section>